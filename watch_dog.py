import os
import subprocess
import smtplib
import requests
from jinja2 import Template
from email.mime.text import MIMEText

from py_tiny_config.config import Config


def send_email(smtp_server, mail_user, mail_password, recipients, subject, body):
    server_ssl = smtplib.SMTP_SSL(smtp_server, 465)
    server_ssl.ehlo()  # optional
    server_ssl.login(mail_user, mail_password)

    msg = MIMEText(body)
    msg['From'] = '<{}>'.format(mail_user)
    msg['To'] = str(", ".join(recipients))
    msg['Subject'] = '{}'.format(subject)

    server_ssl.sendmail(mail_user, recipients, msg.as_string())
    server_ssl.close()

    print('Email sent!')



def watch(config):
    email_from = config['email']
    pwd = config['password']
    subject = config['subject']
    recipients = config['recipients']
    smtp_server = config['smtp_server']

    jninja_message_template = config['jninja_message_template']

    health_urls_to_watch = config['health_urls_to_watch']
    notify_only_on_fail = config['notify_only_on_fail']

    result = {}

    was_fail = False
    for url in health_urls_to_watch:
        median_result = {}
        try:
            r = requests.get(url=url)
            status_code = r.status_code
            median_result['status_code'] = status_code
            if status_code == requests.codes.ok:
                data = r.json()
                median_result['data'] = data
            else:
                was_fail = True
        except Exception as e:
            was_fail = True
            median_result['Error'] = str(e)

        result[url] = median_result

    if not notify_only_on_fail \
            or (notify_only_on_fail and was_fail):

        template = Template(jninja_message_template)
        body = template.render(scope=result)

        send_email(smtp_server, email_from, pwd, recipients, subject, body)
        print('mail sent', body)

    print('done', result)


def main():
    import sys
    if len(sys.argv) < 1:
        print('First parameter should be config filepath!')
    else:
        config_path = sys.argv[1]
        config = Config(config_path)
        watch(config)


if __name__ == "__main__":
    # execute only if run as a script
    main()
