**Watch dog for simple get request and notify to email**

This watch dog try to get request to urls and send notification to email


Sample of usage

Clone it 
git clone https://miko1a@bitbucket.org/miko1a/py_watch_dog.git
cd ./py_watch_dog
git submodule init 
git submodule update

Setup dependencies pip3 install -r requirements.txt
 
Setup config.

And run on linux repitedly every 15 minutes: 
	watch -n 900 python3 watch_dog.py watch_dog.cfg
	run forever:
	nohup watch -n 30 -t  python3 ./watch_dog.py watch_dog.cfg &
	for kill: 
		ps -A # remember watch process pid 
		and kill <pid>
	